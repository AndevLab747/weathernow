package com.yaandrey.weathernow.ui.main.fragments.domain

data class DateTimeDay(
    val date: String,
    val time: String
)