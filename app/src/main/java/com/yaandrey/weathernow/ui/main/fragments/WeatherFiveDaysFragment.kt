package com.yaandrey.weathernow.ui.main.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.yaandrey.weathernow.App
import com.yaandrey.weathernow.R
import com.yaandrey.weathernow.common.BaseFragment
import com.yaandrey.weathernow.data.HasConnectionInternet
import com.yaandrey.weathernow.di.ViewModelFactory
import com.yaandrey.weathernow.ui.main.fragments.adapter.WeatherFiveDaysAdapter
import com.yaandrey.weathernow.ui.main.fragments.di.WeatherFiveDaysComponent
import com.yaandrey.weathernow.utils.observe
import kotlinx.android.synthetic.main.fragment_weather_five_days.*
import javax.inject.Inject

class WeatherFiveDaysFragment : BaseFragment() {

    companion object {
        fun newInstance() = WeatherFiveDaysFragment()
    }

    @Inject
    lateinit var factory: ViewModelFactory

    private val viewModel: FiveDaysWeatherViewModel by viewModels { factory }

    private lateinit var weatherWeekAdapter: WeatherFiveDaysAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WeatherFiveDaysComponent
            .init((requireActivity().application as App).appComponent)
            .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_weather_five_days, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        observers()
    }

    override fun onStart() {
        super.onStart()
        getWeather()
    }

    private fun getWeather() {
        showProgress(getString(R.string.load_weather))
        if (HasConnectionInternet.hasConnection(requireContext())) {
            viewModel.getTheWeatherInFiveDays()
        } else {
            viewModel.getWeatherFromDb()
        }
    }

    private fun observers() {
        observe(viewModel.data) {
            city_name.text = if (it.isEmpty()) "Not found" else it[0].cityName
            weatherWeekAdapter.show(it)
            hideProgress()
            materialCardView.visibility = View.VISIBLE
        }
        observe(viewModel.error) {
            showError("Ошибка при загрузке")
            hideProgress()
        }
    }

    private fun initRecyclerView() {
        weatherWeekAdapter = WeatherFiveDaysAdapter()
        recycler_view.apply {
            adapter = weatherWeekAdapter
            layoutManager = GridLayoutManager(requireContext(), 3)
        }
    }
}