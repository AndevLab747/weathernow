package com.yaandrey.weathernow.ui.main.fragments.storage.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.yaandrey.weathernow.storage.entity.ResponseEntity
import com.yaandrey.weathernow.ui.main.fragments.domain.DateTimeDay

@Entity(tableName = "weatherInSixteenDays", primaryKeys = ["cityId","temp","date","time"])
data class WeatherFiveDaysEntity(
    var cityId: Long,
    val cityName: String,
    val date: String,
    val time: String,
    val temp: Double,
    val icon: String
): ResponseEntity()