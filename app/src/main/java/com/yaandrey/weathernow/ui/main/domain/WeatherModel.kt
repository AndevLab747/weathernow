package com.yaandrey.weathernow.ui.main.domain

import com.yaandrey.weathernow.domain.model.Model

data class WeatherModel(
    val id: Long,
    val name: String,
    val icon: String,
    val temp: Double,
    val feelsLike: Double
): Model()