package com.yaandrey.weathernow.ui.main.di

import com.yaandrey.weathernow.di.AppComponent
import com.yaandrey.weathernow.di.ScreenScope
import com.yaandrey.weathernow.ui.main.MainFragment
import dagger.Component

@ScreenScope
@Component(
    dependencies = [AppComponent::class],
    modules = [MainModule::class, MainViewModelModel::class]
)
interface MainComponent {

    fun inject(mainFragment: MainFragment)

    @Component.Builder
    interface Builder {
        fun appComponent(appComponent: AppComponent): Builder
        fun build(): MainComponent
    }

    companion object {
        fun init(appComponent: AppComponent): MainComponent {
            return DaggerMainComponent.builder()
                .appComponent(appComponent)
                .build()
        }
    }
}