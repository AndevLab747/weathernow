package com.yaandrey.weathernow.ui.main.di

import androidx.lifecycle.ViewModel
import com.yaandrey.weathernow.di.ScreenScope
import com.yaandrey.weathernow.di.ViewModelKey
import com.yaandrey.weathernow.ui.main.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface MainViewModelModel {

    @ScreenScope
    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun bindsMainViewModel(viewModel: MainViewModel): ViewModel
}