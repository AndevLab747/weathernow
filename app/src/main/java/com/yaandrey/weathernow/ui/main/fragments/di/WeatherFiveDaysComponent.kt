package com.yaandrey.weathernow.ui.main.fragments.di

import com.yaandrey.weathernow.di.AppComponent
import com.yaandrey.weathernow.di.ScreenScope
import com.yaandrey.weathernow.ui.main.fragments.WeatherFiveDaysFragment
import dagger.Component

@ScreenScope
@Component(
    dependencies = [AppComponent::class],
    modules = [WeatherFiveDaysModule::class, WeatherFiveDaysViewModelModel::class]
)
interface WeatherFiveDaysComponent {

    fun inject(weatherWeekFragment: WeatherFiveDaysFragment)

    @Component.Builder
    interface Builder {
        fun appComponent(appComponent: AppComponent): Builder
        fun build(): WeatherFiveDaysComponent
    }

    companion object {
        fun init(appComponent: AppComponent): WeatherFiveDaysComponent {
            return DaggerWeatherFiveDaysComponent
                .builder()
                .appComponent(appComponent)
                .build()
        }
    }
}