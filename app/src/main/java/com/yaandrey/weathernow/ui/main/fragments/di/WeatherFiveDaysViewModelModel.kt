package com.yaandrey.weathernow.ui.main.fragments.di

import androidx.lifecycle.ViewModel
import com.yaandrey.weathernow.di.ScreenScope
import com.yaandrey.weathernow.di.ViewModelKey
import com.yaandrey.weathernow.ui.main.fragments.FiveDaysWeatherViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface WeatherFiveDaysViewModelModel {

    @ScreenScope
    @Binds
    @IntoMap
    @ViewModelKey(FiveDaysWeatherViewModel::class)
    fun bindsWeatherWeekViewModel(weatherWeekViewModel: FiveDaysWeatherViewModel): ViewModel
}