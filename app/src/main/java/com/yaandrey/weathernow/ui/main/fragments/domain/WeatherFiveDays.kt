package com.yaandrey.weathernow.ui.main.fragments.domain

import com.yaandrey.weathernow.domain.model.Model

data class WeatherFiveDays(
    var cityId: Long,
    val dateTime: DateTimeDay,
    val cityName: String,
    val temp: Double,
    val icon: String
): Model()