package com.yaandrey.weathernow.ui.main.fragments.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yaandrey.weathernow.R
import com.yaandrey.weathernow.ui.main.fragments.WeatherFiveDaysViewHolder
import com.yaandrey.weathernow.ui.main.fragments.domain.WeatherFiveDays

class WeatherFiveDaysAdapter : RecyclerView.Adapter<WeatherFiveDaysViewHolder>() {

    private val weatherWeekList = ArrayList<WeatherFiveDays>()

    fun show(list: List<WeatherFiveDays>) {
        this.weatherWeekList.clear()
        this.weatherWeekList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherFiveDaysViewHolder =
        WeatherFiveDaysViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_weather, parent, false))

    override fun getItemCount(): Int = weatherWeekList.size

    override fun onBindViewHolder(holder: WeatherFiveDaysViewHolder, position: Int) = holder.bind(weatherWeekList[position])
}