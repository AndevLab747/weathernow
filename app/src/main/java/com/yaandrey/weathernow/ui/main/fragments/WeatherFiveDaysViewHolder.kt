package com.yaandrey.weathernow.ui.main.fragments

import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestListener
import com.yaandrey.weathernow.ICON_URL
import com.yaandrey.weathernow.R
import com.yaandrey.weathernow.ui.main.fragments.domain.WeatherFiveDays
import kotlinx.android.synthetic.main.item_weather.view.*

class WeatherFiveDaysViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val tvCityName: AppCompatTextView = view.city_name
    private val tvDate: AppCompatTextView = view.date
    private val tvTime: AppCompatTextView = view.time
    private val tvTemperature: AppCompatTextView = view.temperature_day
    private val ivIconWeather: AppCompatImageView = view.icon_weather

    fun bind(item: WeatherFiveDays) {
        tvCityName.text = item.cityName
        tvDate.text = item.dateTime.date
        tvTime.text = item.dateTime.time
        tvTemperature.text = itemView.context.getString(R.string.temp, item.temp)
        Glide.with(itemView)
            .load(String.format(ICON_URL, item.icon))
            .placeholder(itemView.context.resources.getDrawable(R.drawable.icon_image))
            .into(ivIconWeather)

    }
}