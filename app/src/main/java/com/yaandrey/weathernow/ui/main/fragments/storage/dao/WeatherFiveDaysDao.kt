package com.yaandrey.weathernow.ui.main.fragments.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.yaandrey.weathernow.ui.main.fragments.storage.entity.WeatherFiveDaysEntity
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface WeatherFiveDaysDao {

    @Query("select * from weatherInSixteenDays where cityId = :cityId order by date, time")
    fun getWeatherList(cityId: Long): Flowable<List<WeatherFiveDaysEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertList(list: List<WeatherFiveDaysEntity>): Completable
}