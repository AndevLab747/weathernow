package com.yaandrey.weathernow.ui.main.fragments

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.yaandrey.weathernow.api.dto.weatherFiveDays.CityWeatherFiveDays
import com.yaandrey.weathernow.data.NetworkMapperWeatherFiveDays
import com.yaandrey.weathernow.data.StorageMapperWeatherFiveDays
import com.yaandrey.weathernow.domain.repository.storage.DataRepository
import com.yaandrey.weathernow.domain.repository.network.NetworkRepository
import com.yaandrey.weathernow.domain.repository.sharedPreferences.SharedPreferenceRepository
import com.yaandrey.weathernow.ui.main.fragments.domain.WeatherFiveDays
import com.yaandrey.weathernow.utils.SingleLiveData
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class FiveDaysWeatherViewModel @Inject constructor (
    private val db: DataRepository,
    private val network: NetworkRepository,
    private val prefs: SharedPreferenceRepository
) : ViewModel() {

    private val errorMut: MutableLiveData<Unit> = SingleLiveData()
    private val dataMut = MutableLiveData<List<WeatherFiveDays>>()

    val error: LiveData<Unit> get() = errorMut
    val data: LiveData<List<WeatherFiveDays>> get() = dataMut

    fun getTheWeatherInFiveDays() {
        network.loadWeatherWeek(prefs.cityId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = {
                    insertDb(it)
                },
                onError = {
                    it.printStackTrace()
                    it.message
                    errorMut.value = Unit
                },
                onComplete = {  }
            )
    }

    private fun insertDb(cityWeatherWeek: CityWeatherFiveDays) {
        db.insertList(NetworkMapperWeatherFiveDays.map(cityWeatherWeek))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onComplete = {
                    getWeatherFromDb()
                },
                onError = {
                    it.stackTrace
                    it.printStackTrace()
                    it.message
                }
            )
    }

    fun getWeatherFromDb() {
        db.getWeatherWeek(prefs.cityId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = {
                    val result = StorageMapperWeatherFiveDays.map(it)
                    dataMut.postValue(result)
                },
                onError = {
                    it.printStackTrace()
                    errorMut.value = Unit
                },
                onComplete = {
                }
            )
    }

}