package com.yaandrey.weathernow.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.yaandrey.weathernow.api.dto.CityWeatherDto
import com.yaandrey.weathernow.data.NetworkMapperWeather
import com.yaandrey.weathernow.data.StorageMapperWeather
import com.yaandrey.weathernow.domain.repository.storage.DataRepository
import com.yaandrey.weathernow.domain.repository.network.NetworkRepository
import com.yaandrey.weathernow.domain.repository.sharedPreferences.SharedPreferenceRepository
import com.yaandrey.weathernow.ui.main.domain.WeatherModel
import com.yaandrey.weathernow.ui.main.fragments.domain.City
import com.yaandrey.weathernow.utils.SingleLiveData
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers.io
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val db: DataRepository,
    private val networkRepository: NetworkRepository,
    private val prefs: SharedPreferenceRepository
) : ViewModel() {

    private val listOfCities = listOf(
        City(524901, "Москва"),
        City(5128638, "Нью-йорк"),
        City(5601462, "Мореленд"),
        City(6608587, "Бледный"),
        City(6608762, "Лапино")
    )

    private val completeLoadingMut = MutableLiveData<Boolean>()
    private val errorMut: MutableLiveData<Unit> = SingleLiveData()
    private val dataMut = MutableLiveData<WeatherModel>()
    private val citiesArrayMut: SingleLiveData<List<City>> = SingleLiveData()

    val citiesArray: LiveData<List<City>> get() = citiesArrayMut
    val completeLoading: LiveData<Boolean> get() = completeLoadingMut
    val error: LiveData<Unit> get() = errorMut
    val data: LiveData<WeatherModel> get() = dataMut

    fun initNow() {
        citiesArrayMut.postValue(listOfCities)
    }

    fun onRefresh() {
        networkRepository.loadWeather(prefs.cityId)
            .subscribeOn(io())
            .observeOn(mainThread())
            .subscribeBy(
                onNext = {
                    insertDb(it)
                },
                onError = {
                    it.printStackTrace()
                    it.message
                    errorMut.value = Unit
                },
                onComplete = { completeLoadingMut.postValue(false) }
            )
    }

    private fun insertDb(cityWeatherDto: CityWeatherDto) {
        db.insert(NetworkMapperWeather.map(cityWeatherDto))
            .subscribeOn(io())
            .observeOn(mainThread())
            .subscribeBy(
                onComplete = {
                    getWeather()
                },
                onError = {
                    it.stackTrace
                    it.printStackTrace()
                    it.message
                }
            )
    }

    fun getWeather() {
        db.getWeather(prefs.cityId)
            .observeOn(mainThread())
            .subscribeBy(
                onNext = {
                    val result = StorageMapperWeather.map(it)
                    dataMut.postValue(result)
                },
                onError = {
                    it.printStackTrace()
                    errorMut.value = Unit
                },
                onComplete = {

                }
            )
    }

    fun setPrefs(cityId: Long, cityName: String) {
        prefs.cityId = cityId
        prefs.cityName = cityName
    }

}