package com.yaandrey.weathernow.ui.main.fragments.domain

data class City(
    var cityId: Long,
    var cityName: String
)