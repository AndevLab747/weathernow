package com.yaandrey.weathernow.ui.main.fragments.di

import android.content.Context
import com.yaandrey.weathernow.api.Api
import com.yaandrey.weathernow.di.ScreenScope
import com.yaandrey.weathernow.domain.repository.storage.DataRepository
import com.yaandrey.weathernow.domain.repository.network.NetworkRepository
import com.yaandrey.weathernow.domain.repository.network.NetworkRepositoryImpl
import com.yaandrey.weathernow.domain.repository.sharedPreferences.SharedPreferenceRepository
import com.yaandrey.weathernow.domain.repository.sharedPreferences.SharedPreferenceRepositoryImpl
import com.yaandrey.weathernow.domain.repository.storage.DataRepositoryImpl
import com.yaandrey.weathernow.storage.AppDatabase
import com.yaandrey.weathernow.ui.main.fragments.storage.dao.WeatherFiveDaysDao
import dagger.Module
import dagger.Provides

@Module
class WeatherFiveDaysModule {

    @ScreenScope
    @Provides
    fun provideWeatherInSixteenDaysDao(appDatabase: AppDatabase): WeatherFiveDaysDao = appDatabase.weatherIntSixteenDaysEntity()

    @ScreenScope
    @Provides
    fun provideDataBaseRepository(database: AppDatabase): DataRepository =
        DataRepositoryImpl(
            database
        )

    @ScreenScope
    @Provides
    fun provideNetworkRepository(api: Api): NetworkRepository =
        NetworkRepositoryImpl(
            api
        )

    @ScreenScope
    @Provides
    fun provideSharedPreferenceRepository(context: Context): SharedPreferenceRepository =
        SharedPreferenceRepositoryImpl(
            context
        )
}