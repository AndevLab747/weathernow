package com.yaandrey.weathernow.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.yaandrey.weathernow.App
import com.yaandrey.weathernow.ICON_URL
import com.yaandrey.weathernow.R
import com.yaandrey.weathernow.common.BaseFragment
import com.yaandrey.weathernow.data.HasConnectionInternet
import com.yaandrey.weathernow.di.ViewModelFactory
import com.yaandrey.weathernow.ui.main.di.MainComponent
import com.yaandrey.weathernow.ui.main.fragments.domain.City
import com.yaandrey.weathernow.utils.observe
import kotlinx.android.synthetic.main.fragment_main.*
import javax.inject.Inject

class MainFragment : BaseFragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    @Inject
    lateinit var factory: ViewModelFactory

    private val viewModel: MainViewModel by viewModels { factory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainComponent
            .init((requireActivity().application as App).appComponent)
            .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listeners()
        observers()
    }

    override fun onStart() {
        super.onStart()
        viewModel.initNow()
        getWeather()
    }

    private fun listeners() {
        button_more_days_weathers.setOnClickListener { findNavController().navigate(R.id.action_mainFragment_to_weatherOnWeekFragment) }
        refresh_layout_main.setOnRefreshListener { getWeather() }
    }

    private fun getWeather() {
        if (HasConnectionInternet.hasConnection(requireContext())) {
            viewModel.onRefresh()
        } else {
            viewModel.getWeather()
        }
    }

    private fun observers() {
        observe(viewModel.data) {
            refresh_layout_main.isRefreshing = false
            city_name.text = it.name
            temperature.text = getString(R.string.temp, it.temp)
            Glide
                .with(this)
                .load(String.format(ICON_URL, it.icon))
                .placeholder(requireContext().resources.getDrawable(R.drawable.icon_image))
                .into(icon_weather)
        }
        observe(viewModel.error) {
            showError("Ошибка при загрузке")
            refresh_layout_main.isRefreshing = false
        }
        observe(viewModel.completeLoading) {
            refresh_layout_main.isRefreshing = it
        }
        observe(viewModel.citiesArray) {
            initChoice(it)
        }
    }

    private fun initChoice(list: List<City>) {
        val listOfCitiesNamed = ArrayList<String>()
        list.forEach { listOfCitiesNamed.add(it.cityName) }
        val adapter = ArrayAdapter(requireContext(), R.layout.list_item, listOfCitiesNamed)
        spinner_choice_city.adapter = adapter
//        spinner_choice_city.setSelection(0)
        spinner_choice_city.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val item = list[position]
                viewModel.setPrefs(item.cityId, item.cityName)
            }
        }
    }



}