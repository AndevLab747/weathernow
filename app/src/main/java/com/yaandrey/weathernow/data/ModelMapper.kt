package com.yaandrey.weathernow.data

import com.yaandrey.weathernow.domain.model.Model

abstract class ModelMapper<From: Model, To> {
    abstract fun map(from: From): To
}