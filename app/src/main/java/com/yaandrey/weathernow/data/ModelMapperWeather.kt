package com.yaandrey.weathernow.data

import com.yaandrey.weathernow.storage.entity.WeatherEntity
import com.yaandrey.weathernow.ui.main.fragments.storage.entity.WeatherFiveDaysEntity
import com.yaandrey.weathernow.ui.main.fragments.domain.WeatherFiveDays
import com.yaandrey.weathernow.ui.main.domain.WeatherModel

object ModelMapperWeatherFifeDays : ModelMapper<WeatherFiveDays, WeatherFiveDaysEntity>() {
    override fun map(from: WeatherFiveDays): WeatherFiveDaysEntity {
        return from.run {
            WeatherFiveDaysEntity(
                cityId,
                cityName,
                dateTime.date,
                dateTime.time,
                temp,
                icon
            )
        }
    }
}

object ModelMapperWeather : ModelMapper<WeatherModel, WeatherEntity>() {
    override fun map(from: WeatherModel): WeatherEntity {
        return from.run { WeatherEntity(id, name, icon, temp, feelsLike) }
    }
}