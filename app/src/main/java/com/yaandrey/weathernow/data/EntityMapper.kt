package com.yaandrey.weathernow.data

import com.yaandrey.weathernow.storage.entity.ResponseEntity

abstract class EntityMapper<From: ResponseEntity, To> {
    abstract fun map(from: From): To
}

abstract class EntityMapperList<From: ResponseEntity, To> {
    abstract fun map(from: List<From>): List<To>
}
