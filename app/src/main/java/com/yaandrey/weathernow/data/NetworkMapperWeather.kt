package com.yaandrey.weathernow.data

import com.yaandrey.weathernow.api.dto.CityWeatherDto
import com.yaandrey.weathernow.api.dto.weatherFiveDays.CityWeatherFiveDays
import com.yaandrey.weathernow.storage.entity.WeatherEntity
import com.yaandrey.weathernow.ui.main.fragments.storage.entity.WeatherFiveDaysEntity
import com.yaandrey.weathernow.utils.toDate
import java.util.*

object NetworkMapperWeather : NetworkMapper<CityWeatherDto, WeatherEntity>() {
    override fun map(from: CityWeatherDto): WeatherEntity {
        return from.run {
            WeatherEntity(id.toLong(), name, weather[0].icon, main.temp, main.feelsLike)
        }
    }
}

object NetworkMapperWeatherFiveDays : NetworkMapperToList<CityWeatherFiveDays, WeatherFiveDaysEntity>() {
    override fun map(from: CityWeatherFiveDays): List<WeatherFiveDaysEntity> {
        val result = ArrayList<WeatherFiveDaysEntity>()
        from.list.forEach {
            val dateTime = it.dtTxt.toDate()
            val temperature = it.main.temp - 273.15
                result.add(
                WeatherFiveDaysEntity(
                    from.city.id.toLong(),
                    from.city.name,
                    dateTime.date,
                    dateTime.time,
                    temperature,
                    it.weather[0].icon
                )
            )
        }
        return result
    }
}