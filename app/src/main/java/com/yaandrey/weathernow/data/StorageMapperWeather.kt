package com.yaandrey.weathernow.data

import com.yaandrey.weathernow.storage.entity.WeatherEntity
import com.yaandrey.weathernow.ui.main.fragments.storage.entity.WeatherFiveDaysEntity
import com.yaandrey.weathernow.ui.main.fragments.domain.WeatherFiveDays
import com.yaandrey.weathernow.ui.main.domain.WeatherModel
import com.yaandrey.weathernow.ui.main.fragments.domain.DateTimeDay

object StorageMapperWeather : EntityMapper<WeatherEntity, WeatherModel>() {
    override fun map(from: WeatherEntity): WeatherModel {
        return from.run { WeatherModel(id, name, icon, temp, feelsLike) }
    }
}

object StorageMapperWeatherFiveDays : EntityMapperList<WeatherFiveDaysEntity, WeatherFiveDays>() {
    override fun map(from: List<WeatherFiveDaysEntity>): List<WeatherFiveDays> {
        val result = ArrayList<WeatherFiveDays>()
        from.forEach {
            result.add(
                WeatherFiveDays(it.cityId, DateTimeDay(it.date, it.time), it.cityName, it.temp, it.icon)
            )
        }
        return result
    }
}