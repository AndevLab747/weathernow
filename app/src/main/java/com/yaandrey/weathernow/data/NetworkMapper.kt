package com.yaandrey.weathernow.data

import com.yaandrey.weathernow.api.dto.ResponseDto

abstract class NetworkMapper<From : ResponseDto, To> {
    abstract fun map(from: From): To
}

abstract class NetworkMapperToList<From : ResponseDto, To> {
    abstract fun map(from: From): List<To>
}