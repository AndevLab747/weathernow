package com.yaandrey.weathernow

object Units {
    const val METRIC = "metric"
    const val IMPERIAL = "imperial"
}

const val ICON_URL = "https://openweathermap.org/img/wn/%s@2x.png"