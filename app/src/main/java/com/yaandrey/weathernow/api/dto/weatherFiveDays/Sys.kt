package com.yaandrey.weathernow.api.dto.weatherFiveDays


import com.google.gson.annotations.SerializedName

data class Sys(
    @SerializedName("pod")
    val pod: String
)