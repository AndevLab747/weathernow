package com.yaandrey.weathernow.api

import com.yaandrey.weathernow.api.dto.CityWeatherDto
import com.yaandrey.weathernow.api.dto.weatherFiveDays.CityWeatherFiveDays
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("weather")
    fun getCurrentWeather(
        @Query("id") id: Long,
        @Query("units") units: String
    ): Observable<CityWeatherDto>


//    @Query("appid") appid: String

    @GET("forecast")
    fun getWeatherInSixteenDays(
        @Query("id") id: Long
    ): Observable<CityWeatherFiveDays>
}