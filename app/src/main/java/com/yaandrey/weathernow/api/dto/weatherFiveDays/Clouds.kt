package com.yaandrey.weathernow.api.dto.weatherFiveDays


import com.google.gson.annotations.SerializedName

data class Clouds(
    @SerializedName("all")
    val all: Int
)