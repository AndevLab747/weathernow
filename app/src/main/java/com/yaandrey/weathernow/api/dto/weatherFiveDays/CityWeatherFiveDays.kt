package com.yaandrey.weathernow.api.dto.weatherFiveDays


import com.google.gson.annotations.SerializedName
import com.yaandrey.weathernow.api.dto.ResponseDto

data class CityWeatherFiveDays(
    @SerializedName("city")
    val city: City,
    @SerializedName("cnt")
    val cnt: Int,
    @SerializedName("cod")
    val cod: String,
    @SerializedName("list")
    val list: List<ListData>,
    @SerializedName("message")
    val message: Double
): ResponseDto()