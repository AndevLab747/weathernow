package com.yaandrey.weathernow.common

interface BaseDialogs {
    fun showProgress(message: String)
    fun updateProgress(progress: Int)
    fun hideProgress()
    fun showError(message: String)
    fun showDialog(title: String, message: String)
}