package com.yaandrey.weathernow.common

import android.annotation.SuppressLint
import android.app.Dialog
import android.util.Log
import android.view.LayoutInflater
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.yaandrey.weathernow.R
import com.yaandrey.weathernow.utils.AlertDialogFragment
import kotlinx.android.synthetic.main.dialog_progress.view.*

abstract class BaseFragment: Fragment(), BaseDialogs {

    override fun showError(message: String) {
        Log.e("ERROR_LOG", "Error: $message")
        val view = view ?: return
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show()
    }

    private var progressDialog: Dialog? = null

    @SuppressLint("InflateParams")
    override fun showProgress(message: String) {
        progressDialog?.dismiss()
        progressDialog = null
        val context = requireContext() ?: return
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_progress, null)
        view.message.text = message
        val dialogBuilder = AlertDialog.Builder(context)
        dialogBuilder.setCancelable(false)
        dialogBuilder.setView(view)
        progressDialog = dialogBuilder.create()
        progressDialog?.show()
    }

    override fun updateProgress(progress: Int) {
        progressDialog?.findViewById<ProgressBar>(R.id.progress_bar)?.let {
            it.isIndeterminate = false
            it.progress = progress
        }
    }

    override fun hideProgress() {
        progressDialog?.dismiss()
        progressDialog = null
    }

    override fun showDialog(title: String, message: String) {
        AlertDialogFragment.instance(title, message).show(childFragmentManager, null)
    }
}