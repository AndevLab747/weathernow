package com.yaandrey.weathernow.di

import android.content.Context
import com.yaandrey.weathernow.api.Api
import com.yaandrey.weathernow.storage.AppDatabase
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton


@Component(modules = [NetworkModule::class, StorageModule::class])
@Singleton
interface AppComponent {

    fun context(): Context
    fun api(): Api
    fun appDataBase(): AppDatabase

    @Component.Builder
    interface Builder {
        fun build(): AppComponent

        @BindsInstance
        fun context(context: Context): Builder
    }
}