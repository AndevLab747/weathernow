package com.yaandrey.weathernow.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.yaandrey.weathernow.storage.entity.WeatherEntity
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface WeatherDao {

    @Query("SELECT * FROM weather WHERE id =:id limit 1")
    fun getWeather(id: Long): Flowable<WeatherEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entity: WeatherEntity): Completable
}