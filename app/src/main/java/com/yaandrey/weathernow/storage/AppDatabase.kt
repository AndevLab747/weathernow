package com.yaandrey.weathernow.storage

import androidx.room.Database
import androidx.room.RoomDatabase
import com.yaandrey.weathernow.storage.dao.WeatherDao
import com.yaandrey.weathernow.ui.main.fragments.storage.dao.WeatherFiveDaysDao
import com.yaandrey.weathernow.storage.entity.WeatherEntity
import com.yaandrey.weathernow.ui.main.fragments.storage.entity.WeatherFiveDaysEntity

@Database(version = 7, exportSchema = false, entities = [WeatherEntity::class, WeatherFiveDaysEntity::class])
abstract class AppDatabase : RoomDatabase() {
    abstract fun weatherDao(): WeatherDao
    abstract fun weatherIntSixteenDaysEntity(): WeatherFiveDaysDao
}