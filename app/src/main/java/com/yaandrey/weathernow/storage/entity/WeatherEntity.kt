package com.yaandrey.weathernow.storage.entity

import androidx.room.Entity

@Entity(tableName = "weather", primaryKeys = ["id"])
data class WeatherEntity(
    var id: Long,
    val name: String,
    val icon: String,
    val temp: Double,
    val feelsLike: Double
): ResponseEntity()