package com.yaandrey.weathernow.domain.repository.network

import com.yaandrey.weathernow.Units
import com.yaandrey.weathernow.api.Api
import com.yaandrey.weathernow.api.dto.CityWeatherDto
import com.yaandrey.weathernow.api.dto.weatherFiveDays.CityWeatherFiveDays
import com.yaandrey.weathernow.domain.repository.network.NetworkRepository
import hu.akarnokd.rxjava3.bridge.RxJavaBridge
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class NetworkRepositoryImpl @Inject constructor (
    private val api: Api
) : NetworkRepository {
    override fun loadWeather(cityId: Long): Observable<CityWeatherDto>   {
        return RxJavaBridge.toV3Observable(api.getCurrentWeather(cityId, Units.METRIC))
    }

    override fun loadWeatherWeek(cityId: Long): Observable<CityWeatherFiveDays> {
        return RxJavaBridge.toV3Observable(api.getWeatherInSixteenDays(cityId))
    }
}