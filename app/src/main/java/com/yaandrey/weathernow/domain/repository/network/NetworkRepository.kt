package com.yaandrey.weathernow.domain.repository.network

import com.yaandrey.weathernow.api.dto.CityWeatherDto
import com.yaandrey.weathernow.api.dto.weatherFiveDays.CityWeatherFiveDays
import io.reactivex.rxjava3.core.Observable

interface NetworkRepository  {
    fun loadWeather(cityId: Long): Observable<CityWeatherDto>
    fun loadWeatherWeek(cityId: Long): Observable<CityWeatherFiveDays>
}