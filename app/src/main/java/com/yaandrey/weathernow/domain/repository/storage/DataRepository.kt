package com.yaandrey.weathernow.domain.repository.storage

import com.yaandrey.weathernow.storage.entity.WeatherEntity
import com.yaandrey.weathernow.ui.main.fragments.storage.entity.WeatherFiveDaysEntity
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable

interface DataRepository {
    fun getWeather(cityId: Long): Flowable<WeatherEntity>
    fun insert(entity: WeatherEntity): Completable
    fun getWeatherWeek(cityId: Long): Flowable<List<WeatherFiveDaysEntity>>
    fun insertList(list: List<WeatherFiveDaysEntity>): Completable
}