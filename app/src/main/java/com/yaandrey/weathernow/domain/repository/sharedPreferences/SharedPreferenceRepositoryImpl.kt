package com.yaandrey.weathernow.domain.repository.sharedPreferences

import android.content.Context
import androidx.preference.PreferenceManager
import com.yaandrey.weathernow.domain.repository.sharedPreferences.SharedPreferenceRepository
import com.yaandrey.weathernow.utils.long
import com.yaandrey.weathernow.utils.string
import javax.inject.Inject

class SharedPreferenceRepositoryImpl @Inject constructor(context: Context) :
    SharedPreferenceRepository {
    private val prefs = PreferenceManager.getDefaultSharedPreferences(context)

    override var cityId: Long by prefs.long()
    override var cityName: String by prefs.string()

}