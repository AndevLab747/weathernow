package com.yaandrey.weathernow.domain.repository.sharedPreferences

interface SharedPreferenceRepository {

    var cityId: Long
    var cityName: String
}