package com.yaandrey.weathernow.domain.repository.storage

import com.yaandrey.weathernow.domain.repository.storage.DataRepository
import com.yaandrey.weathernow.storage.AppDatabase
import com.yaandrey.weathernow.storage.entity.WeatherEntity
import com.yaandrey.weathernow.ui.main.fragments.storage.entity.WeatherFiveDaysEntity
import hu.akarnokd.rxjava3.bridge.RxJavaBridge
import io.reactivex.rxjava3.core.*
import javax.inject.Inject

class DataRepositoryImpl @Inject constructor(
    private val db: AppDatabase
) : DataRepository {

    override fun getWeather(cityId: Long): Flowable<WeatherEntity> {
        return RxJavaBridge.toV3Flowable(db.weatherDao().getWeather(cityId))
    }

    override fun insert(entity: WeatherEntity): Completable {
        return RxJavaBridge.toV3Completable(db.weatherDao().insert(entity))
    }

    override fun getWeatherWeek(cityId: Long): Flowable<List<WeatherFiveDaysEntity>> {
        return RxJavaBridge.toV3Flowable(db.weatherIntSixteenDaysEntity().getWeatherList(cityId))
    }

    override fun insertList(list: List<WeatherFiveDaysEntity>): Completable {
        return RxJavaBridge.toV3Completable(db.weatherIntSixteenDaysEntity().insertList(list)) 
    }
}