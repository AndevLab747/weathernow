package com.yaandrey.weathernow.utils

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

fun <T> AppCompatActivity.observe(data: LiveData<T>, eventCallBack: (T) -> Unit) {
    data.observe(this, Observer { event ->
        event?.let {
            eventCallBack(event)
        }
    })
}

fun <T> Fragment.observe(data: LiveData<T>, eventCallBack: (T) -> Unit) {
    data.observe(this, Observer { event ->
        event?.let {
            eventCallBack(event)
        }
    })
}