package com.yaandrey.weathernow.utils

import com.yaandrey.weathernow.ui.main.fragments.domain.DateTimeDay
import java.text.SimpleDateFormat
import java.util.*

fun String.toDate(): DateTimeDay {
    val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    val formatDate = sdf.parse(this)
    val sdfDate = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
    val date = sdfDate.format(formatDate)
    val sdfTime = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
    val time = sdfTime.format(formatDate)
    return DateTimeDay(date, time)
}