package com.yaandrey.weathernow

import android.app.Application
import com.facebook.stetho.Stetho
import com.yaandrey.weathernow.di.AppComponent
import com.yaandrey.weathernow.di.DaggerAppComponent

class App : Application() {

    lateinit var appComponent: AppComponent


    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .context(this)
            .build()
        initMonitoring()
    }

    private fun initMonitoring() {
        Stetho.initialize(
            Stetho.newInitializerBuilder(this)
                .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                .build()
        )
    }
}